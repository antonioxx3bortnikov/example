import { Typography } from 'components/Typography'
import styles from './style.module.scss'
import cn from 'clsx'

const data = [
  'Кроссфит',
  'Пилатес',
  'Йогатерапия',
  'Хахта-Йога',
  'Виньяса',
  'Кундалини',
  'Релаксация'
]

export const Lessons = () => {
  const activeItem = data[3]
  return (
    <div className={styles.wrap}>
      <div className={styles.headTitle}>
        <Typography tag="h1" fontStyle="h1">
          Что ты получаешь?
        </Typography>
        <div>
          <Typography tag="h4" fontStyle="h4" className={styles.description}>
            Более
            <Typography tag="span" fontStyle="h3" color="var(--primary-100)">
              &nbsp;500&nbsp;
            </Typography>
            занятий
          </Typography>
          <Typography tag="p" fontStyle="p1" className={styles.descriptionInfo}>
            В любое время в любом месте
          </Typography>
        </div>
      </div>
      <div className={styles.groupLessons}>
        {data.map((item, key) => {
          return (
            <div
              className={cn(styles.lessonItem, activeItem === item ? styles.active : '')}
              key={key}
            >
              <Typography
                tag="p"
                fontStyle="h6"
                color="var(--primary-100)"
                className={styles.lessonName}
              >
                {item}
              </Typography>
            </div>
          )
        })}
      </div>
    </div>
  )
}
