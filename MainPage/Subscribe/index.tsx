import React from 'react'
import { Typography } from 'components/Typography'
import styles from './style.module.scss'
import Image from 'next/image'
import cn from 'clsx'

export const Subscribe = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.image}>
          <Image src="/images/main-page/human.png" layout="fill" alt="human" />
        </div>
        <div className={styles.description}>
          <Typography
            tag="h3"
            fontStyle="h3"
            className={cn(styles.descriptionBg, styles.descriptionText)}
          >
            1 месяц подписки EVO
            <Typography
              tag="span"
              fontStyle="span"
              color="var(--primary-100)"
              className={styles.descriptionText}
            >
              8
            </Typography>
          </Typography>
          <Typography tag="h3" fontStyle="h3" className={styles.descriptionText}>
            <Typography
              tag="span"
              fontStyle="span"
              color="var(--primary-100)"
              className={styles.descriptionText}
            >
              дешевле
            </Typography>
            , чем 1 оффлайн занятие в зале или студии
          </Typography>
        </div>
      </div>
    </div>
  )
}
