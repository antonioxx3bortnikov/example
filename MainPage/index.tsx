import { Banner } from './Banner'
import { Choise } from './Choise'
import { Device } from './Device'
import { Instructors } from './Instructors'
import { Reviews } from './Reviews'
import { Subscribe } from './Subscribe'
import { ITrainer } from '@interfaces/trainer'
import { FC } from 'react'
import Head from 'next/head'

interface MainPageProps {
  trainers: ITrainer[]
}

export const MainPage: FC<MainPageProps> = ({ trainers }) => {
  return (
    <>
      <Head>
        <title>
          Онлайн йога, видео уроки от 475 руб в месяц, тренировки с EVO8 - evo8.ru
        </title>
        <meta
          name="description"
          content="evo8.ru - EVO8 - онлайн йога, 5 дней - бесплатно! Более 304 видео тренировок по йоге онлайн, новые уроки каждую неделю! Тренируйся с лучшими инструкторами в любое удобное время. Будь здоровым и сильным вместе с EVO8!"
        />
      </Head>
      <Banner />
      <Choise />
      <Device />
      <Instructors trainers={trainers} />
      <Reviews />
      <Subscribe />
    </>
  )
}
