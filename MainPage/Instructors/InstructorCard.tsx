import { ITrainer } from '@interfaces/trainer'
import { Typography } from 'components/Typography'
import Image from 'next/image'
import { FC } from 'react'

import styles from './style.module.scss'

interface InstructorCardProps {
  trainer: ITrainer
  navigate: () => void
}

export const InstructorCard: FC<InstructorCardProps> = ({ trainer, navigate }) => {
  const { preview_image, name, surname, description } = trainer
  return (
    <div className={styles.card} onClick={navigate}>
      <div className={styles.cardImage}>
        <Image src={preview_image} layout="fill" alt="Name" />
      </div>
      <div className={styles.cardTitle}>
        <Typography tag="h6" fontStyle="h6">
          {name}
        </Typography>
        <Typography tag="h6" fontStyle="h6">
          {surname}
        </Typography>
      </div>
      <div className={styles.cardDescription}>
        <Typography tag="p" fontStyle="p3">
          {description.substring(0, 50) + '...'}
        </Typography>
      </div>
    </div>
  )
}
