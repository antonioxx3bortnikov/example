import styles from './style.module.scss'
import { ITrainer } from '@interfaces/trainer'
import { Typography } from 'components/Typography'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { FC, useRef } from 'react'
import { CustomCarousel } from '@features/shared/components/UI/CustomCarousel'
import { useManageScreen } from '@hooks/useManageScreen'
import { InstructorCard } from './InstructorCard'
import Link from 'next/link'

const responsive = {
  xxl: {
    breakpoint: { max: 3000, min: 1900 },
    items: 7
  },
  xl: {
    breakpoint: { max: 1900, min: 1600 },
    items: 6
  },
  desktop: {
    breakpoint: { max: 1600, min: 1100 },
    items: 4
  },
  tablet: {
    breakpoint: { max: 1100, min: 800 },
    items: 3
  },
  mobile: {
    breakpoint: { max: 800, min: 0 },
    items: 2
  }
}

interface InstructorsProps {
  trainers: ITrainer[]
}

export const Instructors: FC<InstructorsProps> = ({ trainers }) => {
  const { isDesktop } = useManageScreen()
  const router = useRouter()

  const carouselRef = useRef(null)
  const carouselSecondRef = useRef(null)

  const navigateById = (id: number) => () => router.push(`/trainers/${id}`)
  const trainersPart1 = [...trainers]
  const trainersPart2 = trainersPart1.splice(
    trainersPart1.length / 2,
    trainersPart1.length - 1
  )
  return (
    <div className={styles.wrap}>
      <Typography tag="h3" fontStyle="h3" className={styles.headTitle}>
        Тренируйся с лучшими
      </Typography>
      <div className={styles.headDescription}>
        <Typography tag="p" fontStyle="p1">
          Высокий профессионализм и авторские методики в работе каждого инструктора
          подарят тебе полное ощущение индивидуальных занятий
        </Typography>
      </div>
      <div className={styles.headDescription}></div>
      <div className={styles.cardGroup}>
        {!!trainersPart1.length && isDesktop ? (
          <CustomCarousel
            ref={carouselRef}
            autoPlay
            autoPlaySpeed={20}
            centerMode={false}
            customTransition="all 20s linear"
            draggable
            infinite
            pauseOnHover
            responsive={responsive}
            shouldResetAutoplay
            swipeable
            transitionDuration={20000}
          >
            {trainersPart1.map((trainer) => (
              <InstructorCard
                key={trainer.id}
                trainer={trainer}
                navigate={navigateById(trainer.id)}
              />
            ))}
          </CustomCarousel>
        ) : (
          trainersPart1.map((trainer) => (
            <InstructorCard
              key={trainer.id}
              trainer={trainer}
              navigate={navigateById(trainer.id)}
            />
          ))
        )}
      </div>
      <div className={styles.cardGroup}>
        {!!trainersPart2.length && isDesktop ? (
          <CustomCarousel
            ref={carouselRef}
            autoPlay
            autoPlaySpeed={20}
            centerMode={false}
            customTransition="all 20s linear"
            draggable
            infinite
            pauseOnHover
            responsive={responsive}
            shouldResetAutoplay
            swipeable
            transitionDuration={20000}
            rtl={true}
          >
            {trainersPart2.map((trainer) => (
              <InstructorCard
                key={trainer.id}
                trainer={trainer}
                navigate={navigateById(trainer.id)}
              />
            ))}
          </CustomCarousel>
        ) : (
          trainersPart2.map((trainer) => (
            <InstructorCard
              key={trainer.id}
              trainer={trainer}
              navigate={navigateById(trainer.id)}
            />
          ))
        )}
      </div>
      <button className={styles.cardButton}>
        <Link href="/trainers">
          <a target="_blank">
            <Typography tag="span" fontStyle="p1">
              Познакомиться с командой
            </Typography>
          </a>
        </Link>
      </button>
    </div>
  )
}
