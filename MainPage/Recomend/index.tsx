import styles from './style.module.scss'
import cn from 'clsx'
import { Typography } from 'components/Typography'
import Image from 'next/image'
import { useRouter } from 'next/router'
import React from 'react'
import { MamaLogo, RelaxLogo, YogaLogo } from 'components/icons'

const data = [
  {
    description: 'Расслабляйся, медитируй и спи крепко',
    project: 'relax',
    evoimage: <RelaxLogo width={140} height={60} viewBox="0 0 106 53" />,
    img: '/images/main-page/relax.png',
    link: '/relax'
  },
  {
    description: 'Удели внимание женскому здоровью - это важно!',
    project: 'mama',
    evoimage: <MamaLogo width={140} height={60} viewBox="0 0 106 53" />,
    img: '/images/main-page/mama.png',
    link: 'https://mama.evo8.ru/'
  },
  {
    description: 'Мы создали магазин с товарами, коорые могут вам помочь',
    project: 'yoga',
    evoimage: <YogaLogo width={140} height={60} viewBox="0 0 106 53" />,
    img: '/images/main-page/yoga.png',
    link: '/trainings'
  }
]

export const Recomend = () => {
  const router = useRouter()
  const navigate = (url: string) => () => router.push(url)
  return (
    <div className={styles.wrap}>
      <div className={styles.wrapTitle}>
        <Typography tag="h6" fontStyle="h6" color="var(--primary-100)">
          Предложения для вас
        </Typography>
        <Typography tag="h2" fontStyle="h2">
          Наши продукты
        </Typography>
      </div>
      <div className={styles.article}>
        {data.map(({ description, evoimage, project, img, link }, key) => (
          <div className={styles.card} key={key}>
            <Typography
              tag="p"
              fontStyle="p1"
              className={cn(
                styles.cardTitle,
                project === 'relax' ? styles.cardTitleRelax : ''
              )}
            >
              {description}
            </Typography>
            <div className={styles.projectImage}>{evoimage}</div>
            <div className={styles.cardImage}>
              <Image src={img} layout="fill" alt={description} />
            </div>
            <div className={cn(styles.cardBtn, styles[project])} onClick={navigate(link)}>
              <Typography tag="p" fontStyle="p1">
                Начать
              </Typography>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
