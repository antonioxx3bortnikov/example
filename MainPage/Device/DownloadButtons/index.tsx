import styles from '../style.module.scss'
import { GoogleDownloadIcon, IosDownloadIcon } from 'components/icons'
import Link from 'next/link'

export const DownloadButtons = () => {
  return (
    <div className={styles.groupDownloads}>
      <div className={styles.downloadBtn}>
        <Link href="https://play.google.com/store/apps/details?id=ru.evo8.yoga">
          <a target="_blank">
            <GoogleDownloadIcon />
          </a>
        </Link>
      </div>
      <div className={styles.downloadBtn}>
        <Link
          href="https://apps.apple.com/us/app/new-evo8-%D0%B9%D0%BE%D0%B3%D0%B0-%D0%B8-%D0%BC%D0%B5%D0%B4%D0%B8%D1%82%D0%B0%D1%86%D0%B8%D1%8F/id6443390602"
          passHref
        >
          <a target="_blank">
            <IosDownloadIcon />
          </a>
        </Link>
      </div>
    </div>
  )
}
