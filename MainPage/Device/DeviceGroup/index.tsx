import styles from '../style.module.scss'
import Image from 'next/image'

export const DeviceGroup = () => {
  return (
    <div className={styles.groupImages}>
      <div className={styles.imageTv}>
        <Image src="/images/main-page/tv.webp" layout="fill" alt="tv" />
      </div>
      <div className={styles.imageLaptop}>
        <Image src="/images/main-page/macbook.png" layout="fill" alt="tv" />
      </div>
      <div className={styles.imagePhone}>
        <Image src="/images/main-page/mobile.png" layout="fill" alt="tv" />
      </div>
    </div>
  )
}
