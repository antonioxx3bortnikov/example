import styles from './style.module.scss'
import { Typography } from 'components/Typography'
import { FC } from 'react'
import { DeviceGroup } from './DeviceGroup'
import { DownloadButtons } from './DownloadButtons'
import { TickIcon } from 'components/icons'
import Link from 'next/link'

const data = [
  'Доступ к видеосервису на сайте, в мобильном приложении и Смарт ТВ',
  'Качественная музыка для тренировок',
  'Видео в FullHD качестве',
  'Сервисная поддержка 24/7'
]

export const Device: FC = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.group}>
          <DeviceGroup />
          <DownloadButtons />
        </div>
        <div className={styles.articleText}>
          <div className={styles.articleTitle}>
            <Typography tag="h3" fontStyle="h3">
              На любом устройстве и в любом месте
            </Typography>
          </div>
          {data.map((item, key) => {
            return (
              <div className={styles.description} key={key}>
                <div className={styles.icon}>
                  <TickIcon />
                </div>
                <Typography tag="p" fontStyle="h5">
                  {item}
                </Typography>
              </div>
            )
          })}
          <Link href="/registration">
            <a className={styles.button}>
              <Typography tag="span" fontStyle="p1">
                Начать заниматься
              </Typography>
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
