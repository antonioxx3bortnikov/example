import styles from './style.module.scss'
import { Typography } from 'components/Typography'
import { FC } from 'react'
import Link from 'next/link'
import { useManageScreen } from '@hooks/useManageScreen'

const cost = 475

export const Banner: FC = () => {
  const { isMobile } = useManageScreen()
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        {isMobile ? (
          <div className={styles.image}></div>
        ) : (
          <div className={styles.image}>
            <video muted autoPlay loop>
              <source
                src="/video/intro.webm"
                type="video/webm"
                media="(min-width: 2000px)"
              />
              <source
                src="/video/intro.mp4"
                type="video/mp4"
                media="(min-width: 2000px)"
              />
              <source
                src="/video/intro-md.webm"
                type="video/webm"
                media="(min-width: 768px)"
              />
              <source
                src="/video/intro-md.mp4"
                type="video/mp4"
                media="(min-width: 768px)"
              />
              <source src="/video/intro-md.mp4" type="video/mp4" />
              <source src="/video/intro-sm.webm" type="video/webm" />
              <source src="/video/intro-sm.mp4" type="video/mp4" />
            </video>
          </div>
        )}
        <div className={styles.headTitle}>
          <Typography tag="h3" fontStyle="h3">
            Йога, фитнес и медитация
          </Typography>
          <Typography tag="p" fontStyle="p1" className={styles.description}>
            <Typography
              tag="span"
              fontStyle="h4"
              color="var(--primary-100)"
              className={styles.price}
            >
              500+
            </Typography>
            авторских занятий от инструкторов топ-класса
          </Typography>
          <Typography tag="p" fontStyle="p1" className={styles.underDescription}>
            От
            <Typography
              tag="span"
              fontStyle="h4"
              color="var(--primary-100)"
              className={styles.price}
            >
              {cost}
            </Typography>
            руб. в месяц
          </Typography>
          <Link href="/registration">
            <a
              className={styles.freeBtn}
              onClick={() => {
                VK.Goal('conversion')
                ym(********, 'reachGoal', 'landing_start')
                return true
              }}
            >
              <Typography tag="span" fontStyle="p1">
                Попробовать бесплатно
              </Typography>
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
