import styles from './style.module.scss'
import { Typography } from 'components/Typography'
import Image from 'next/image'
import Link from 'next/link'

const data = [
  {
    title: 'Первые шаги',
    description: 'Начни йогу с нуля и получи базовые знания',
    btnTitle: 'Хочу пройти обучение',
    tag: 'main_first_steps',
    imgUrl: '/images/main-page/choise-beginner.png'
  },
  {
    title: 'Здоровое тело',
    description:
      'Избавься от боли в спине, верни подвижность суставам и продли молодость всего организма',
    btnTitle: 'Хочу улучшить здоровье',
    tag: 'main_healthy_body',
    imgUrl: '/images/main-page/choise-body.png'
  },
  {
    title: 'Расслабление и дыхание',
    description: 'Настройся на хороший сон и забудь про стресс',
    btnTitle: 'Хочу сбросить напряжение',
    tag: 'main_relax',
    imgUrl: '/images/main-page/choise-relax.png'
  },
  {
    title: 'Будь в форме',
    description: 'Худей и совершенствуй свою фигуру с помощью активных тренировок',
    btnTitle: 'Хочу тело мечты',
    tag: 'main_dream_body',
    imgUrl: '/images/main-page/choise-shape.png'
  }
]
export const Choise = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.headTitle}>
        <Typography tag="h3" fontStyle="h3">
          Присоединяйся
        </Typography>
        <div>
          <Typography tag="h4" fontStyle="h4" className={styles.description}>
            Более
            <Typography tag="span" fontStyle="h3" color="var(--primary-100)">
              &nbsp;100 000&nbsp;
            </Typography>
            человек уже
          </Typography>
          <Typography tag="h4" fontStyle="h4" className={styles.description}>
            занимаются с нами
          </Typography>
        </div>
      </div>
      <div className={styles.groupCard}>
        {data.map(({ title, description, btnTitle, imgUrl, tag }, key) => (
          <div className={styles.card} key={key}>
            <div className={styles.cardImage}>
              <Image src={imgUrl} layout="fill" alt={title} />
            </div>
            <div className={styles.cardBg} />
            <div className={styles.cardTitle}>
              <Typography tag="h5" fontStyle="h5">
                {title}
              </Typography>
            </div>
            <div className={styles.cardDescription}>
              <Typography tag="p" fontStyle="p2">
                {description}
              </Typography>
            </div>
            <button
              className={styles.cardBtn}
              onClick={() => {
                ym(********, 'reachGoal', { tag })
                _tmr.push({ id: '********', type: 'reachGoal', goal: tag })
              }}
            >
              <Link href="/registration">
                <a>
                  <Typography tag="p" fontStyle="p3">
                    {btnTitle}
                  </Typography>
                </a>
              </Link>
            </button>
          </div>
        ))}
      </div>
    </div>
  )
}
