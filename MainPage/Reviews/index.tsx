import { Typography } from 'components/Typography'
import styles from './style.module.scss'
import Image from 'next/image'
import cn from 'clsx'
import { CustomCarousel } from '@features/shared/components/UI/CustomCarousel'
import 'react-multi-carousel/lib/styles.css'
import React, { useRef } from 'react'

const data = [
  {
    image: '',
    name: 'volynkinanastasya',
    text: 'Нашла ваш сайт на карантине. А потом подумала: всегда времени не хватает, а здесь - вот время. И попробовала. Очень довольна, теперь каждый день йога. Я в восторге от вашего сайта. А что самое главное, можно заниматься в удобном месте и в удобное время.',
    project: 'evo8yoga',
    reply:
      'Спасибо, нам очень приятно ☺️ У нас скоро выходит обновление, и наша платформа станет еще удобнее для вас❤️'
  },
  {
    image: '',
    name: 'venera_niz',
    text: 'Прозанималась с Вами месяц, очень здорово, что есть возможность и программу выбрать, и музыку, и инструктора! Хочу продлить подписку!',
    project: 'evo8yoga',
    reply:
      'Очень благодарны вам ваш отзыв! Обязательно продлевайте, чтобы не пропускать все наши новые тренировки и медитации, которые регулярно пополняются'
  },
  {
    image: '',
    name: 'wild_vi',
    text: 'Спасибо за лучший онлайн-проект по йоге! Счастья и здоровья вашим тренерам',
    project: 'evo8yoga',
    reply:
      'Спасибо! EVO8 - это большая семья, в которой все заботятся друг о друге, ваш отзыв это подтверждает'
  },
  {
    image: '',
    name: 'massage.oren.ilina',
    text: 'Сейчас увидела новый раздел! Спасибо большое, хоть я благодаря вам не могу себя начинающей назвать, но с большим удовольствием пройду эти 40 шагов. Спасибо большое за вашу работу! Буду проходить и ждать такой раздел для опытных',
    project: 'evo8yoga',
    reply:
      'Спасибо за такие предложения, обязательно учтем ваши пожелания! А пока мы готовим кое-что другое очень интересное для опытных пользователей, вам точно понравится!'
  },
  {
    image: '',
    name: 'irinaenl',
    text: 'Как же мне повезло год назад случайно подписаться на @evo8active с днем рождения! Продолжайте расти и процветать!',
    project: 'evo8yoga',
    reply: 'Спасибо вам❤️ Стараемся для вас!'
  },
  {
    image: '',
    name: 'dashamos_39',
    text: 'Занимаюсь с вами по подписке! Это просто бомба! Много полезного узнала, занимаюсь с удовольствием! Много инструкторов и всё так даступно и понятно! Спасибо за Ваш труд!',
    project: 'evo8yoga',
    reply:
      'Очень вам благодарны ❤️ Если у вас возникают какие-то вопросы, можете всегда написать нам, мы поможем!'
  },
  {
    image: '',
    name: 'surikatamama',
    text: 'После этих тренировок в мышцах ещё долго играет «адреналин». Очень крутые! Спасибо!',
    project: 'evo8yoga',
    reply:
      'Здорово, что на вас так влияют наши тренировки☺️ Но не забывайте о растяжки и о медитации после интенсивных нагрузок, подходящие вы можете найти в разделах: "Гибкость и подвижность" и "Расслабление и дыхание"'
  },
  {
    image: '',
    name: 'irabasyrova',
    text: 'Занимаюсь на вашем сайте, нигде не видела таких шикарных и разнообразных уроков, я с вами на всю жизнь!',
    project: 'evo8yoga',
    reply:
      'Это очень приятно, спасибо☺️ Мы стараемся создать заряженную среду единомышленников, с разнообразными привилегиями внутри сообщества.'
  },
  {
    image: '',
    name: 'podpalavika',
    text: 'Занимаюсь с вами почти пол года. Очень здоров. Это как раз мой формат, можно выбрать тренеровку под настроени и потребности)',
    project: 'evo8yoga',
    reply:
      'Спасибо, мы стараемся для вас! Если у вас возникают идеи по тренировкам, которых у нас еще нет, пишите нам, мы обязательно учтем'
  },
  {
    image: '',
    name: 'm_chainier',
    text: 'Вы создали очень крутой проект! Заниматься с вами - одно удовольствие!',
    project: 'evo8yoga',
    reply: 'Оставайтесь с нами! Дальше - больше!'
  }
]

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1624 },
    items: 3.5
  },
  tablet: {
    breakpoint: { max: 1624, min: 1280 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 1280, min: 0 },
    items: 1
  }
}

export const Reviews = () => {
  const carouselRef = useRef(null)

  const onNextSlide = () => {
    carouselRef?.current?.next()
  }

  const onPrevSlide = () => {
    carouselRef?.current?.previous()
  }

  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.articleText}>
          <Typography tag="h5" fontStyle="h5" color="var(--primary-100)">
            Наши отзывы
          </Typography>
          <div className={styles.title}>
            <Typography tag="h3" fontStyle="h3">
              Что о нас говорят пользователи EVO8
            </Typography>
          </div>
          <div className={styles.description}>
            <Typography tag="p" fontStyle="p1">
              Уже тысячи людей тренируются с нами. И лучше чем они, о нашем сервисе не
              расскажет никто.
            </Typography>
          </div>
          <div className={styles.arrowGroupBtn}>
            <div className={styles.arrowBtn} onClick={onPrevSlide}>
              <div className={styles.arrowIcon}>
                <Image src="/images/general/arrow.png" layout="fill" alt="arrows" />
              </div>
            </div>
            <div className={styles.arrowBtn} onClick={onNextSlide}>
              <div className={cn(styles.arrowIcon, styles.arrowRight)}>
                <Image src="/images/general/arrow.png" layout="fill" alt="arrows" />
              </div>
            </div>
          </div>
        </div>
        <div className={styles.cardGroup}>
          <CustomCarousel ref={carouselRef} responsive={responsive}>
            {data.map(({ image, name, text, project, reply }, key) => (
              <div className={styles.card} key={key}>
                <div className={styles.cardArticle}>
                  <div className={styles.cardName}>
                    {image == '' ? (
                      ''
                    ) : (
                      <div className={styles.icon}>
                        <Image src={image} layout="fill" alt="inst" />
                      </div>
                    )}
                    <Typography tag="h6" fontStyle="h6">
                      {name}
                    </Typography>
                  </div>
                  <Typography tag="p" fontStyle="p1">
                    {text}
                  </Typography>
                </div>
                <div className={styles.cardArticleReply}>
                  <div className={styles.cardName}>
                    <div className={styles.yogaIcon} />
                    <Typography tag="h6" fontStyle="h6">
                      {project}
                    </Typography>
                  </div>
                  <Typography tag="p" fontStyle="p1">
                    {reply}
                  </Typography>
                </div>
              </div>
            ))}
          </CustomCarousel>
        </div>
      </div>
    </div>
  )
}
