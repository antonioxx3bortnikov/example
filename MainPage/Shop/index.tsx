import { Typography } from 'components/Typography'
import Image from 'next/image'
import Link from 'next/link'
import styles from './style.module.scss'

const data = [
  {
    description: 'Топ боксерка черный',
    img: '/images/main-page/bodyset-one.webp',
    link: 'https://shop.evo8.ru/topy-i-legginsy/top-bokserka-chernyi.html'
  },
  {
    description: 'Топ с лямками внахлест на спине бирюзовый',
    img: '/images/main-page/bodyset-two.webp',
    link: 'https://shop.evo8.ru/topy-i-legginsy/top-s-lyamkami-vnahlest-na-spine-biryuzovyi.html'
  },
  {
    description: 'Коврик для йоги non slip розовый',
    img: '/images/main-page/carpet-pink.webp',
    link: 'https://shop.evo8.ru/kovriki/kovrik-dlya-iogi-non-slip-rozovyi.html'
  }
]

export const Shop = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.wrapTitle}>
        <Typography tag="h6" fontStyle="h6" color="var(--primary-100)">
          Наш магазин
        </Typography>
        <Typography tag="h2" fontStyle="h2">
          У тебя все есть для занятий?
        </Typography>
      </div>
      <div className={styles.article}>
        {data.map(({ description, img, link }, key) => (
          <div className={styles.card} key={key}>
            <div className={styles.cardImage}>
              <Image src={img} layout="fill" alt={description} />
            </div>
            <Typography tag="h6" fontStyle="h6" className={styles.cardTitle}>
              {description}
            </Typography>
            <Link href={link}>
              <a target="_blank" className={styles.cardButton}>
                <Typography tag="p" fontStyle="p1">
                  Купить
                </Typography>
              </a>
            </Link>
          </div>
        ))}
      </div>
    </div>
  )
}
