import styles from './style.module.scss'
import { Typography } from 'components/Typography'
import cn from 'clsx'
import { DeviceGroup } from 'components/MainPage/Device/DeviceGroup'

export const UsefulGift = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.articleTitle}>
          <Typography tag="h2" fontStyle="h2" className={styles.articleTitleText}>
            Очень
          </Typography>
          <Typography
            tag="h2"
            fontStyle="h2"
            color="var(--primary-100)"
            className={cn(styles.mr, styles.articleTitleText)}
          >
            Полезный
          </Typography>
          <Typography tag="h2" fontStyle="h2" className={styles.articleTitleText}>
            Подарок
          </Typography>
        </div>
        <Typography tag="p" fontStyle="p1" className={styles.articleDescription}>
          Пусть у ваших близких, друзей и коллег появится больше сил и энергии.
        </Typography>
        <button className={styles.button}>
          <Typography tag="span" fontStyle="p1">
            Купить подписку в подарок
          </Typography>
        </button>
      </div>
      <DeviceGroup />
    </div>
  )
}
