import { Typography } from 'components/Typography'
import styles from './style.module.scss'
import cn from 'clsx'

const data = [
  {
    title: 'Подписка EVO8',
    month: 'На 1 месяц',
    sale: '',
    prevCost: '',
    cost: '790 Р'
  },
  {
    title: 'Подписка EVO8',
    month: 'На 6 месяцев',
    sale: '-51%',
    prevCost: '4756 р.',
    cost: '7790р.'
  },
  {
    title: 'Подписка EVO8',
    month: 'На 12 месяцев',
    sale: '-51%',
    prevCost: '9452 р.',
    cost: '6900р.'
  }
]
export const ChoiseGift = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.articleTitle}>
          <Typography tag="h1" fontStyle="h1" className={styles.title}>
            Выберите
          </Typography>
          <Typography tag="h1" fontStyle="h1" color="var(--primary-100)">
            подарок
          </Typography>
        </div>
        <div className={styles.articleCard}>
          {data.map(({ title, month, sale, prevCost, cost }, key) => (
            <div className={styles.cards} key={key}>
              <div className={styles.cardsTitle}>
                <Typography tag="h5" fontStyle="h5">
                  {title}
                </Typography>
              </div>
              {sale === '' ? (
                <div className={styles.cardsMonth}>
                  <Typography tag="p" fontStyle="p1">
                    {month}
                  </Typography>
                </div>
              ) : (
                <div className={styles.cardsMonth}>
                  <Typography tag="p" fontStyle="p1">
                    {month}
                  </Typography>
                  <div className={styles.monthSale}>{sale}</div>
                </div>
              )}
              {prevCost === '' ? (
                <div className={cn(styles.cardsCost)}>
                  <Typography tag="h2" fontStyle="h2" color="var(--color-red)">
                    {cost}
                  </Typography>
                </div>
              ) : (
                <div className={styles.cardsCost}>
                  <Typography tag="h4" fontStyle="h4" className={styles.prevCost}>
                    {prevCost}
                  </Typography>
                  <Typography tag="h2" fontStyle="h2" color="var(--color-red)">
                    {cost}
                  </Typography>
                </div>
              )}
              <div className={styles.cardsBtn}>Купить</div>
            </div>
          ))}
        </div>
        <button className={styles.button}>
          <Typography tag="span" fontStyle="p1">
            Купить подписку в подарок
          </Typography>
        </button>
      </div>
    </div>
  )
}
