import styles from './style.module.scss'
import Image from 'next/image'
import { Typography } from 'components/Typography'

const data = [
  {
    img: '/images/Gift/adaptive-1.png',
    description:
      'Наши занятия подходят каждому! С нами очень легко начать продуктивно заниматься!'
  },
  {
    img: '/images/Gift/adaptive-2.png',
    description:
      'У нас есть готовые программы для начинающих, в которых наши опытные инструкторы просто и понятно помогут разобраться с основами в йоге.'
  },
  {
    img: '/images/Gift/adaptive-3.png',
    description:
      'Специальные тренировки для опытных помогут улучшить навыки и прочувствовать свое тело по-новому.'
  }
]
export const Adaptive = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.articleTitle}>
          <Typography tag="h3" fontStyle="h3">
            Подходит для опытных и тех , кто делает только первые шаги!
          </Typography>
        </div>
        <div className={styles.articleCards}>
          {data.map(({ img, description }, key) => {
            return (
              <div className={styles.cards} key={key}>
                <div className={styles.cardsImg}>
                  <Image src={img} layout="fill" alt="first" />
                </div>
                <div className={styles.cardsDescription}>
                  <Typography tag="p" fontStyle="p1">
                    {description}
                  </Typography>
                </div>
              </div>
            )
          })}
        </div>
        <button className={styles.button}>
          <Typography tag="span" fontStyle="p1">
            Купить подписку в подарок
          </Typography>
        </button>
      </div>
    </div>
  )
}
