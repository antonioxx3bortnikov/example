import styles from './style.module.scss'
import Image from 'next/image'
import { Typography } from 'components/Typography'
import { ScissorsIcon } from 'components/icons'

export const CertGift = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.articleBanner}>
          <div className={styles.articleImg}>
            <Image src="/images/Gift/abramova.png" layout="fill" alt="abramova" />
          </div>
        </div>
        <div className={styles.articleText}>
          <div className={styles.articleTitle}>
            <Typography tag="h2" fontStyle="h2" color="var(--primary-100)">
              Подарочный Сертификат
            </Typography>
            <Typography tag="h2" fontStyle="h2">
              на онлайн йогу
            </Typography>
          </div>
          <div className={styles.articleBtn}>
            <div className={styles.articleBtnImg}>
              <ScissorsIcon
                width={30}
                height={30}
                viewBox="0 0 30 30"
                color="var(--primary-100)"
              />
            </div>
            <div className={styles.btnDescription}>
              <Typography tag="p" fontStyle="p2">
                Промокод на скидку
              </Typography>
              <Typography tag="p" fontStyle="p1" className={styles.promoCode}>
                PODAROK21
              </Typography>
            </div>
          </div>
        </div>
        <div className={styles.articleBannerDescription}>
          <Typography tag="p" fontStyle="p2">
            Нашу подписку можно подарить в любой день, Особенно 14 февраля, 8 марта и в
            день рождения!
          </Typography>
        </div>
      </div>
    </div>
  )
}
