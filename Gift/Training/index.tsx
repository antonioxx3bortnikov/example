import styles from './style.module.scss'
import Image from 'next/image'
import { Typography } from 'components/Typography'
import { LampChargeIcon } from 'components/icons'

const data = [
  'Улучшают качество сна',
  'Снижают стресс',
  'Делают спину здоровой',
  'Помогают обрести стройное тело',
  'Заряжают положительной энергией',
  'Восстанавливают силы баланс'
]

export const Training = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.articleImg}>
          <Image src="/images/Gift/abramova-2.png" layout="fill" alt="articleImg" />
        </div>
        <div className={styles.articleText}>
          <div className={styles.articleTitle}>
            <Typography tag="h3" fontStyle="h3">
              Наши тренировки и программы:
            </Typography>
          </div>
          <div className={styles.articlePositive}>
            {data.map((item, key) => {
              return (
                <div className={styles.articleDescription} key={key}>
                  <div className={styles.descriptionImg}>
                    <LampChargeIcon width={30} height={30} viewBox="0 0 25 25" />
                  </div>
                  <Typography tag="p" fontStyle="p2">
                    {item}
                  </Typography>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}
