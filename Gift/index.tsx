import { Adaptive } from './Adaptive'
import { CertGift } from './CertGift'
import { UsefulGift } from './UsefulGift'
import { ChoiseGift } from './ChoiseGift'
import { Training } from './Training'

export const Gift = () => {
  return (
    <>
      <UsefulGift />
      <CertGift />
      <ChoiseGift />
      <Training />
      <Adaptive />
    </>
  )
}
