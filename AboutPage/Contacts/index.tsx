import styles from './style.module.scss'
import { AboutNavigation } from '../AboutUs/Navigation'

export const Contacts = () => {
  return (
    <div className={styles.wrap}>
      <AboutNavigation titlePage="Контакты" />
    </div>
  )
}
