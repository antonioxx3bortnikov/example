import {
  PartnersOzonIcon,
  PartnersTele2Icon,
  PartnersTinkoffIcon,
  PartnersVkIcon,
  PartnersYandexIcon,
  PartnersRostelecomIcon
} from 'components/icons'

export const Logo = [
  { name: <PartnersVkIcon width={120} height={38} /> },
  { name: <PartnersYandexIcon width={120} height={38} /> },
  { name: <PartnersTinkoffIcon width={120} height={38} /> },
  { name: <PartnersRostelecomIcon width={120} height={38} /> },
  { name: <PartnersOzonIcon width={120} height={38} /> },
  { name: <PartnersTele2Icon width={120} height={38} /> }
]
