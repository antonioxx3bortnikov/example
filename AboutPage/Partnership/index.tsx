import styles from './style.module.scss'
import { AboutNavigation } from '../AboutUs/Navigation'
import { Typography } from 'components/Typography'
import { Logo } from './logo'

export const Partnership = () => {
  return (
    <div className={styles.wrap}>
      <AboutNavigation titlePage="Сотрудничество" />
      <div className={styles.article}>
        <div className={styles.articlePartners}>
          <div className={styles.partnersText}>
            <Typography tag="h3" fontStyle="h3" className={styles.partnersTitle}>
              Давайте будем хорошими друзьями и надежными партнерами!
            </Typography>
            <Typography tag="p" fontStyle="p1" className={styles.partnersDescription}>
              Ваша компания создает продукт, связанный со здоровым образом жизни? Готова к
              интеграциям? Есть интересное предложение для нас? — Команда EVO8 открыта для
              диалога! Напишите нам на privet@evo8.ru
            </Typography>
            <Typography tag="p" fontStyle="p1" className={styles.partnersSubDescription}>
              Мы уже дружим и создаем совместные проекты с такими компаниями
            </Typography>
          </div>
          <div className={styles.partners}>
            {Logo.map(({ name }, key) => {
              return (
                <div className={styles.partnersImg} key={key}>
                  <div className={styles.partnersLogo}>{name}</div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}
