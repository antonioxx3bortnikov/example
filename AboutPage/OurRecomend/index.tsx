import styles from './style.module.scss'
import Image from 'next/image'
import { AboutNavigation } from '../AboutUs/Navigation'
import { Typography } from 'components/Typography'
import { DownloadButtons } from './DownloadButtons'
import { MamaLogo, RelaxLogo, YogaLogo } from 'components/icons'

const data = [
  {
    description: 'Занимайся йогой и будь в хорошей форме',
    evoimage: <YogaLogo width={140} height={60} viewBox="0 0 106 53" />,
    img: '/images/main-page/yoga-second.png',
    urlGoogle: 'https://play.google.com/store/apps/details?id=com.evo8.app&hl=ru&gl=Ru',
    urlIos:
      'https://apps.apple.com/ru/app/evo8-%D0%B9%D0%BE%D0%B3%D0%B0-%D1%84%D0%B8%D1%82%D0%BD%D0%B5%D1%81-%D0%B4%D0%BE%D0%BC%D0%B0-%D0%B0%D1%81%D0%B0%D0%BD%D0%B0/id6443390602'
  },
  {
    description: 'Расслабляйся, медитируй и спи крепко',
    evoimage: <RelaxLogo width={140} height={60} viewBox="0 0 106 53" />,
    img: '/images/main-page/relax.png',
    urlGoogle: 'https://play.google.com/',
    urlIos: 'https://apple.com/'
  },
  {
    description: 'Удели внимание женскому здоровью - это важно!',
    evoimage: <MamaLogo width={140} height={60} viewBox="0 0 106 53" />,
    img: '/images/main-page/mama.png',
    urlGoogle: 'https://mama.evo8.ru/',
    urlIos: 'https://mama.evo8.ru/'
  }
]
export const OurRecomend = () => {
  return (
    <div className={styles.wrap}>
      <AboutNavigation titlePage="Наши предложения" />
      <div className={styles.article}>
        <div className={styles.articleText}>
          <Typography tag="h2" fontStyle="h2" className={styles.articleTitle}>
            Наши приложения
          </Typography>
          <Typography tag="p" fontStyle="p1">
            Мы активная команда, которая очень любит развиваться, поэтому мы решили
            реализовать удобный комплекс для тех, кто тренируется вместе с нами. Мы
            активная команда, которая очень любит развиваться, поэтому мы решили
            реализовать удобный комплекс для тех, кто тренируется вместе с нами. Мы
            активная команда, которая очень любит развиваться, поэтому мы решили
            реализовать удобный комплекс для тех, кто тренируется вместе с нами
          </Typography>
        </div>
        <div className={styles.articleImg}>
          <div className={styles.imageIphoneLeft}>
            <Image
              src="/images/about-us/Iphone12ProLeft.png"
              layout="fill"
              alt="Apple Iphone 12 Pro"
            />
          </div>
          <div className={styles.imageIphoneCenter}>
            <Image
              src="/images/about-us/Iphone12ProCenter.png"
              layout="fill"
              alt="Apple Iphone 12 Pro"
            />
          </div>
          <div className={styles.imageIphoneRight}>
            <Image
              src="/images/about-us/Iphone12ProRight.png"
              layout="fill"
              alt="Apple Iphone 12 Pro"
            />
          </div>
          <div className={styles.circleShadow} />
        </div>
      </div>
      <div className={styles.articleCard}>
        {data.map(({ description, evoimage, img, urlGoogle, urlIos }, key) => (
          <div key={key}>
            <div className={styles.card}>
              <Typography tag="h6" fontStyle="h6" className={styles.cardTitle}>
                {description}
              </Typography>
              <div className={styles.projectImage}>{evoimage}</div>
              <div className={styles.cardImage}>
                <Image src={img} layout="fill" alt={description} />
              </div>
            </div>
            <DownloadButtons urlGoogle={urlGoogle} urlIos={urlIos} />
          </div>
        ))}
      </div>
    </div>
  )
}
