import styles from '../style.module.scss'
import { GoogleDownloadIcon, IosDownloadIcon } from 'components/icons'
import Link from 'next/link'

export const DownloadButtons = ({ urlGoogle = '/', urlIos = '/' }) => {
  return (
    <div className={styles.groupDownloads}>
      <div className={styles.downloadBtn}>
        <Link href={urlGoogle}>
          <a target="_blank">
            <GoogleDownloadIcon />
          </a>
        </Link>
      </div>
      <div className={styles.downloadBtn}>
        <Link href={urlIos}>
          <a target="_blank">
            <IosDownloadIcon />
          </a>
        </Link>
      </div>
    </div>
  )
}
