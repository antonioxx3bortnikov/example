export const Data = [
  {
    imgUrl: '/images/about-us/arhipova-006 1.png',
    title: 'Привет!',
    subTitle: 'Мы - ',
    titleImg: 'logo',
    description:
      'Наша команда стремится дать каждому возможность развивать себя ментально и физически.',
    style: '1'
  },
  {
    imgUrl: '/images/about-us/arhipova-006 2.png',
    title: 'Нам нравится вдохновлять',
    description:
      'EVO8 объединяет людей в сообщество, которые хотят быть здоровыми и успешными.',
    style: '2'
  },
  {
    imgUrl: '/images/about-us/arhipova-006 3.png',
    title: 'И вы мотивируете нас делать больше',
    description:
      ' Каждый из вас становится лучше, благодаря собственным силам и желаниям! Наши продукты просто помогают вам в этом.',
    style: '1'
  },
  {
    imgUrl: '/images/about-us/arhipova-006 4.png',
    title: 'Наш секрет',
    description: 'Мы что-то создаем и даем это людям.',
    style: '2'
  }
]
