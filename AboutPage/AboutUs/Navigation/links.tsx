export const Links = [
  {
    title: 'О нас',
    url: '/about'
  },
  {
    title: 'Для инструкторов',
    url: '/forinstructors'
  },
  {
    title: 'Сотрудничество',
    url: '/partnership'
  },
  {
    title: 'Контакты',
    url: '/contacts'
  }
]
