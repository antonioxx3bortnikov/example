import styles from '../style.module.scss'
import { Typography } from 'components/Typography'
import Link from 'next/link'
import cn from 'clsx'
import { Links } from './links'

export const AboutNavigation = ({ titlePage = '' }) => {
  return (
    <div className={styles.navigation}>
      {Links.map(({ title, url }, key) => (
        <Link href={url} key={key}>
          <a
            className={cn(
              styles.navigationLink,
              titlePage === title ? styles.active : ''
              // isActive === 'active' ? styles.active : ''
            )}
          >
            <Typography tag="p" fontStyle="p1">
              {title}
            </Typography>
          </a>
        </Link>
      ))}
    </div>
  )
}
