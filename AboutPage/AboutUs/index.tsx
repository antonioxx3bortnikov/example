import styles from './style.module.scss'
import { Typography } from 'components/Typography'
import Image from 'next/image'
import cn from 'clsx'
import { DeviceGroup } from 'components/MainPage/Device/DeviceGroup'
import { AboutNavigation } from './Navigation'
import { Data } from './articleData'
import { DownloadButtons } from '../OurRecomend/DownloadButtons'
import { useManageScreen } from '@hooks/useManageScreen'
import { YogaCompact } from 'components/icons'

export const About = () => {
  const { isDesktop } = useManageScreen()
  return (
    <div className={styles.wrap}>
      <AboutNavigation titlePage="О нас" />
      {Data.map(({ imgUrl, title, subTitle, titleImg, description, style }, key) => (
        <div
          className={cn(styles.article, style === '2' ? styles.articleReverse : '')}
          key={key}
        >
          <div className={styles.articleImg}>
            <Image src={imgUrl} layout="fill" alt="arhipova-1" />
          </div>
          <div
            className={cn(
              styles.articleText,
              style === '2' ? styles.textRight : styles.textLeft
            )}
          >
            {titleImg ? (
              <div className={styles.articleTitle}>
                <Typography tag="h3" fontStyle="h3">
                  {title}
                </Typography>
                <Typography tag="h3" fontStyle="h3" className={styles.subTitle}>
                  {subTitle}
                  <div className={styles.titleImg}>
                    {isDesktop ? (
                      <YogaCompact width={134} height={38} viewBox="0 0 134 38" />
                    ) : (
                      <YogaCompact width={124} height={30} viewBox="0 0 134 38" />
                    )}
                  </div>
                </Typography>
              </div>
            ) : (
              <div className={styles.articleTitle}>
                <Typography tag="h3" fontStyle="h3">
                  {title}
                </Typography>
              </div>
            )}
            <div className={styles.articleDescription}>
              <Typography tag="p" fontStyle="p1">
                {description}
              </Typography>
            </div>
          </div>
        </div>
      ))}
      <div className={styles.devices}>
        <Typography tag="h3" fontStyle="h3" className={styles.devicesTitle}>
          Мы доступны на любом устройстве
        </Typography>
        <DownloadButtons
          urlGoogle="https://play.google.com/store/apps/details?id=com.evo8.app&hl=ru&gl=Ru"
          urlIos="https://apps.apple.com/ru/app/evo8-%D0%B9%D0%BE%D0%B3%D0%B0-%D1%84%D0%B8%D1%82%D0%BD%D0%B5%D1%81-%D0%B4%D0%BE%D0%BC%D0%B0-%D0%B0%D1%81%D0%B0%D0%BD%D0%B0/id6443390602"
        />
        <DeviceGroup />
      </div>
    </div>
  )
}
