import styles from './style.module.scss'
import { Typography } from 'components/Typography'
import Image from 'next/image'
import cn from 'clsx'
import { AboutNavigation } from '../AboutUs/Navigation'
export const ForInstructors = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.article}>
        <div className={styles.artileLeft}>
          <AboutNavigation titlePage="Для инструкторов" />
          <div className={styles.articleText}>
            <Typography tag="h1" fontStyle="h1" className={styles.articleTitle}>
              Сотрудничество
            </Typography>
            <Typography tag="h4" fontStyle="h4" className={styles.articleSubTitle}>
              Инструкторам
            </Typography>
            <Typography tag="p" fontStyle="p1">
              Совместный контент с нами – это большая работа! Поэтому Мы приветствуем
              каждого инструктора по йоге, кто готов создавать вдохновляющие и уникальные
              практики для наших пользователей. Если вы не боитесь камер и можете
              продемонстрировать авторский подход к делу, то напишите нам на
              privet@evo8.ru
            </Typography>
          </div>
          <div className={styles.articleTextImg}>
            <div className={styles.textImg}>
              <Image src="/images/about-us/about-one.png" layout="fill" alt="about-one" />
            </div>
            <div className={styles.textImg}>
              <Image src="/images/about-us/about-two.png" layout="fill" alt="about-one" />
            </div>
          </div>
        </div>
        <div className={styles.articleRight}>
          <div className={styles.articleImg}>
            <div className={styles.articleImgLeft}>
              <div className={styles.articleImgSmall}>
                <Image
                  src="/images/about-us/about-three.png"
                  layout="fill"
                  alt="about-one"
                />
              </div>
              <div className={cn(styles.articleImgMedium, styles.marginTop)}>
                <Image
                  src="/images/about-us/about-five.png"
                  layout="fill"
                  alt="about-one"
                />
              </div>
            </div>
            <div className={styles.articleImgRight}>
              <div className={styles.articleImgMedium}>
                <Image
                  src="/images/about-us/about-four.png"
                  layout="fill"
                  alt="about-one"
                />
              </div>
              <div className={cn(styles.articleImgSmall, styles.marginTop)}>
                <Image
                  src="/images/about-us/about-seven.png"
                  layout="fill"
                  alt="about-one"
                />
              </div>
            </div>
          </div>
          <div className={cn(styles.articleImgDown, styles.marginTop)}>
            <div className={styles.articleImgHigh}>
              <Image src="/images/about-us/about-six.png" layout="fill" alt="about-one" />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
